import json


# JSON 文件路径
def get_config():
    file_path = "./data/config/panel.json"

    # 打开 JSON 文件并读取数据
    with open(file_path, "r") as json_file:
        # 使用 json 模块的 load() 方法将 JSON 数据解析为 Python 对象
        json_data = json.load(json_file)
    file_path = "./data/config/ssh.json"

    # 打开 JSON 文件并读取数据
    with open(file_path, "r") as json_file:
        # 使用 json 模块的 load() 方法将 JSON 数据解析为 Python 对象
        json_data1 = json.load(json_file)
        json_data["ssh_config"] = json_data1
    # 打印 JSON 对象
    return json_data
