from app.utils import File
import datetime
import time
import os


def New(text):
    # 获取当前日期和时间
    current_datetime = datetime.datetime.now()
    # 提取年、月、日
    year = str(current_datetime.year)
    month = str(current_datetime.month)
    day = str(current_datetime.day)
    File.Add(os.getcwd()+ "/data/log/" + year + "-" + month + "-" + day +".txt", str({"time": time.time(), "text": text}) + "\n")