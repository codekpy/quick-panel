#!/bin/bash

# 下载文件
file_url="https://cn-sy1.rains3.com/stardream/linux/linux_0.1.1.tar.xz"
file_name=$(basename $file_url)
wget $file_url

# 解压文件
tar -xf $file_name

# 检查 Python 3 是否已安装
python_version=$(python3 --version 2>&1)

if [[ $python_version == *"Python 3"* ]]; then
    echo "Python 3 is already installed."
else
    echo "Python 3 is not installed. Installing..."

    # 检测当前 Linux 发行版
    source /etc/os-release

    # 根据发行版选择安装工具
    if [[ $ID == "debian" || $ID == "ubuntu" ]]; then
        sudo apt-get update
        sudo apt-get install python3
    elif [[ $ID == "centos" || $ID == "rhel" ]]; then
        sudo yum install python3
    else
        echo "Unsupported Linux distribution. Please install Python 3 manually."
    fi
fi
