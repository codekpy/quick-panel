//当页面完全加载时
window.onload = function() {
    if (localStorage.getItem('page')){
        turn(localStorage.getItem('page'))
    }else{
        turn('1')
    }
}

function turn (id) {
	// 设置所有 class 为 .menu-item 的元素背景为 #233E79
	var menuItems = document.getElementsByClassName('menu-item');
	for (var i = 0; i < menuItems.length; i++) {
		menuItems[i].style.backgroundColor = '#233E79';
	}

	var elementWithId1 = document.getElementById(id);
	if (elementWithId1) {
		elementWithId1.style.backgroundColor = '#407AFF';
	}

	// 设置页面中 <iframe> 的 src 属性为 ./test
	var iframeElement = document.querySelector('iframe');
	if (iframeElement) {
		if (id == '1') {
			iframeElement.src = '/p/home';
		}else if(id == '2'){
		    iframeElement.src = '/p/site';
		}else if(id == '3'){
		    iframeElement.src = '/p/db';
		}else if(id == '4'){
		    iframeElement.src = '/p/safe';
		}
	}
	localStorage.setItem('page', id)
}