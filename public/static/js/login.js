document.addEventListener("DOMContentLoaded", function() {
  // 监听表单提交事件
  document.querySelector("form").addEventListener("submit", function(event) {
    // 阻止默认表单提交行为
    event.preventDefault();

    // 获取用户名和密码
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // 构造要发送的数据对象
    var data = {
      username: username,
      pw: password
    };

    // 发送 POST 请求
    fetch("/api/post/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
      if (data.code === 200) {
        // 登录成功，可以进行相应的操作，比如跳转页面等
        console.log("登录成功");
        window.location.href = "/"; // 示例：跳转到仪表盘页面
      } else {
        // 登录失败，处理错误
        console.error("登录失败");
        alert("登录失败，请检查用户名和密码！");
      }
    })
    .catch(error => {
      // 发生网络错误或其他异常
      console.error("发生错误", error);
      alert("发生错误，请稍后重试！");
    });
  });
});
