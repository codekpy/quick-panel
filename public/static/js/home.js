window.onload = function() {
    // 使用 Fetch API 发起 GET 请求
    function runtime_state(){

        fetch('/api/get/runtime_state')
          .then(function(response) {
            // 当收到响应时，将其转换为 JSON
            return response.json();
          })
          .then(function(data) {
            // 处理获取到的数据
            console.log(data);
            //在这处理

            //循环4次
            var injectedHTML = "<h4>状态</h4>"
            let number = 0;
            for (let i = 0; i < 2; i++) {
                if (i==0){
                    cpu_core = data.cpu_core;
                    cpu_usage = data.cpu_usage;
                    cpu = cpu_usage / cpu_core;
                    proportion = cpu;
                    console.log("CPU使用率：" + cpu);
                    number = parseInt(cpu * 100);
                    text = "CPU占用"
                }else{
                    memory_total = data.memory_total;
                    memory_usage = data.memory_usage;
                    memory = memory_usage / memory_total;
                    proportion = memory;
                    console.log("内存使用率：" + memory_usage);
                    number = parseInt(memory_usage);
                    text = "内存占用"
                }
                injectedHTML += `         <div>
                <svg viewBox="0 0 100 100">
                    <path
                        d="M 50 50 m 0 -46 a 46 46 0 1 1 0 92 a 46 46 0 1 1 0 -92"
                        stroke="#F4F8FF"
                        stroke-width="6.5px"
                        fill="none"
                        style="stroke-dasharray: 289.027, 289.027; stroke-dashoffset: 0px"
                    />
                    <path
                        d="M 50 50 m 0 -46 a 46 46 0 1 1 0 92 a 46 46 0 1 1 0 -92"
                        stroke="#69A0FF"
                        fill="none"
                        stroke-linecap="round"
                        stroke-width="6.5px"
                        style='stroke-dasharray: `+ proportion* 289.027 +`, 289.027; stroke-dashoffset: 0px; transition: stroke-dasharray 0.5s ease 0s, stroke 0.5s ease 0s, opacity 0.5s ease 0s'
                    />
                </svg>

                <span style='top: 1rem;
                    left: 0;
                    width: 40%;
                    textAlign: center;
                    margin: 0;
                    transform: translateY(-50%);
                '>
                    <div>
                        <span>
                            <div style="fontWeight: bold;color: #000;fontSize: 25px;">
                               ` + number + `%
                            </div>

                            <div style="margin-top: 3.2px;color: #000,fontSize: 12px;">
                                ` + text + `
                            </div>
                        </span>
                    </div>
                </span>
            </div>`
            }


            // 获取 id 为 "status" 的元素
            var statusElement = document.getElementById('status');

            // 将 HTML 注入到元素中
            if (statusElement) {
                statusElement.innerHTML = '';
                statusElement.innerHTML += injectedHTML;
            } else {
                console.error('找不到id为"status"的元素');
            }

          })
          .catch(function(error) {
            // 处理错误
            console.error('发生错误:', error);
          });
    }
    runtime_state()
    setInterval(runtime_state, 5000)

    fetch('/api/get/system_info')
      .then(function(response) {
        // 当收到响应时，将其转换为 JSON
        return response.json();
      })
      .then(function(data) {
        // 处理获取到的数据
        // 获取 id 为 system 的元素
        var systemElement = document.getElementById('system');
        systemElement.textContent = data.data;
      })
      .catch(function(error) {
        // 处理错误
        console.error('发生错误:', error);
      });
}