@echo off
set DOWNLOAD_URL=https://cn-sy1.rains3.com/stardream/windows/win_1.2.0.zip
set DOWNLOAD_PATH=C:\Program Files\win_1.2.0.zip
set EXTRACT_PATH=C:\Program Files\quick-panel
set MAIN_EXE_PATH=%EXTRACT_PATH%\main.exe

echo 正在下载文件...
curl -o "%DOWNLOAD_PATH%" %DOWNLOAD_URL%

echo 正在解压文件...
mkdir "%EXTRACT_PATH%"
tar -xf "%DOWNLOAD_PATH%" -C "%EXTRACT_PATH%"

echo 正在以管理员权限运行 main.exe...
cd /d "%EXTRACT_PATH%"
powershell Start-Process "main.exe" -Verb RunAs

echo 执行完毕！
