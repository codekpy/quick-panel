## 🚀 QuickPanel
[![star](https://gitee.com/codekpy/quick-panel/badge/star.svg?theme=dark)](https://gitee.com/codekpy/quick-panel/stargazers)
## 📝 简介
QuickPanel 是使用 Python 和 Flask 框架构建的服务器管理仪表板。

## 🏗️ 软件架构
QuickPanel 后端采用 Python 3.8 开发，前端使用 Node.js 和 Vue3 构建。

## 📖 使用指南
详细的使用说明请访问 [QPanel 官方网站](https://qpanel.cloudroo.top/)。

## 🤝 参与贡献
如果您想为 QuickPanel 做出贡献，请按照以下步骤：
1. Fork 该仓库。
2. 创建一个名为 `Feat_xxx` 的新分支。
3. 提交您的代码更改。
4. 创建一个新的 Pull Request。

您的贡献将不胜感激！