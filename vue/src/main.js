import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ArcoVue from '@arco-design/web-vue';
import '@arco-themes/vue-qpanel/css/arco.css';
import 'https://lf1-cdn-tos.bytegoofy.com/obj/iconpark/icons_32830_7.193889457c17c5f06e2b1c3099e47f60.js'



const app = createApp(App)
app.use(router)
app.use(ArcoVue);
app.mount('#app')
