import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'defult',
      component: HomeView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/db',
      name: 'db',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/DB.vue')
    },
    {
      path: '/site',
      name: 'site',
      component: () => import('../views/Site.vue')
    },
    {
      path: '/set',
      name: 'set',
      components: {
        default:() => import('../views/Set.vue'),
        panel:() => import('../components/SetPanel.vue')
      },
      children:[
        {
          path: 'panel',
          component: () => import('../components/SetPanel.vue')
        }
      ],
    },
    {
      path: '/ftp',
      name: 'ftp',
      component: () => import('../views/Ftp.vue')
    },
    {
      path: '/soft',
      name: 'soft',
      component: () => import('../views/Soft.vue')
    },
    {
      path: '/shell',
      name: 'shell',
      component: () => import('../components/Shell.vue')
    },
    {
      path: '/safe',
      name: 'safe',
      components: {
        default:() => import('../views/Safe.vue'),
        port:() => import('../components/SafePort.vue')
      },
      children:[
        {
          path: 'port',
          component: () => import('../components/SafePort.vue')
        }
      ],
    },
    {
      path: '/file',
      name: 'file',
      component: () => import('../views/File.vue')
    },
  ]
})

export default router
